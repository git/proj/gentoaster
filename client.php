<?php

    // Gentoaster build daemon client
    // Licensed under GPL v3, see COPYING file

    require_once "config.php";

    if (!isset($argv[1])) {
        die("No config file provided\n");
    }

    $client= new GearmanClient();
    $client->addServer();

    echo "Sending job\n";

    $iniString = file_get_contents($argv[1]);

    $handle = $client->doBackground("invoke_image_build", $iniString);
    $handlehash = md5($handle);

    echo "Job sent, handle was ".$handle." - hash ".$handlehash."\n";

    $db = new mysqli(
        MYSQL_HOSTNAME,
        MYSQL_USERNAME,
        MYSQL_PASSWORD,
        MYSQL_DATABASE
    );
    
    if (mysqli_connect_errno()) {
       die("Could not connect to database ".mysqli_connect_error());
    }

    $query = "INSERT INTO builds (id, handle) VALUES(?, ?)";
    $stmt = $db->prepare($query);
    $stmt->bind_param("ss", $handlehash, $handle);
    $stmt->execute();
    $stmt->close();
    $db->close();
    echo "Job handle mapping added to database\n";