<?php
	require_once "config.php";
	require_once GENTOASTER_PATH."/ui/status.php";
?>
<html>
    <head>
        <title>Gentoaster</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css"
          href="css/ui-lightness/jquery-ui-1.8.14.custom.css">
        <script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.14.js"></script>
        <?php if ($inprogress) { ?>
        <script>
          var UUID = "<?php echo $buildID; ?>"; 
        
          $(document).ready(function() {
            $("#progressbar").progressbar({ value: <?php echo $percentage; ?> });
          });
          
          function bar_update() {
                $.getJSON('ajax.php?uuid='+UUID, function(data) {
                    if(data.status == 1) {
                        percent = Math.floor((data.num/data.den)*100);
                        $("#progressbar").progressbar({ value: percent });
                        $("#percent").html(percent);
                    } else {
                        window.location.reload();
                    }
                });
                setTimeout("bar_update();", 3000);
          }
          
          bar_update();
        </script>
        <?php } ?>
        
    </head>
    <body>
        <div id="container">
            <div id="header"></div>
            <div id="content">
                <div id="main">
                    <div id="status" class="step">
                        <h1><?php echo $titleString; ?></h1>
                        <p>
                            <?php echo $simultaneousString; ?>
                            <?php echo $bres; ?>
                            <div id="progressbar"></div>
                        </p>
                    </div>
                </div>
                <div id="navigation">

                </div>
            </div>
        </div>
        </script>
    </body>
</html>