<?php
	require_once "config.php";
	require_once GENTOASTER_PATH."/ui/index.php";
?>
<html>
    <head>
        <title>Gentoaster</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css"
          href="css/ui-lightness/jquery-ui-1.8.14.custom.css">
        <script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.form.js"></script>
        <script type="text/javascript" src="js/jquery.validate.js"></script>
        <script type="text/javascript" src="js/bbq.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.14.js"></script>
        <script type="text/javascript" src="js/jquery.wizard.js"></script>
        <script type="text/javascript">
            var RecaptchaOptions = {
                theme : 'clean'
            };
         </script>
    </head>
    <body>
        <div id="container">
            <div id="header"></div>
            <form id="wizard" method="post" action="process.php" class="bbq">
                <div id="content">
                    <div id="main">
                        <div id="welcome" class="step">
                            <h1>Welcome to Gentoaster!</h1>
                            <p>This wizard will guide you through the creation 
                            of your own personalised Gentoo virtual machine 
                            image.</p>
                        </div>
                        <?php if(SMTP_ENABLED) { ?>
                        <div id="notifications" class="step">
                            <h1>Notifications</h1>
                            Want us to email you when your image has finished toasting? 
                            Pop your email in here. If you don't want an email, simply 
                            leave this field blank.<br /><br />
                            <label for="notifications_email">E-Mail</label>
                            <br />
                            <input type="text" name="email" id="notifications_email">
                        </div>
                        <?php } ?>
                        <?php
                            if (RECAPTCHA_ENABLED) {
                        ?>
                        <div id="human" class="step">
                            <h1>Verification</h1>

                            <?php
                             echo recaptcha_get_html(RECAPTCHA_PUBLIC_KEY);
                            ?>
                            <br>
                        </div>
                        <?php
                            }
                        ?>
                        <div id="locale" class="step">
                            <h1>Locale</h1>

                            <label for="locale_language">Language</label>
                            <br />
                            <select id="locale_language" class="required"
                                name="language">
                                <option>English (UK)</option>
                            </select>
                            <br>

                            <label for="locale_keyboard">Keyboard Layout</label>
                            <br />
                            <select id="locale_keyboard" class="required"
                                name="keyboard">
                                <?php echo $keyboardOption; ?>
                            </select>
                            <br>

                            <label for="locale_timezone">Timezone</label>
                            <br />
                            <select id="locale_timezone" class="required"
                                name="timezone">
                                <?php echo $timezoneOption; ?>
                            </select>
                            <br>
                        </div>
                        <div id="user" class="step">
                            <h1>User details</h1>

                            <label for="user_username">Username</label><br />
                            <input id="user_username" class="required"
                                type="text" name="username"><br>

                            <label for="user_password">Password</label><br />
                            <input id="user_password" class="required"
                                type="password" name="password">
                                
                            <input id="user_confirmpassword" class="required"
                                type="password" name="confirmpassword"><br>

                            <label for="user_rootpassword">Root Password</label>
                            <br />
                            <input id="user_rootpassword" class="required"
                                type="password" name="rootpassword">
                                
                            <input id="user_confirmrootpassword"
                                class="required" type="password"
                                name="confirmrootpassword"><br>
                        </div>
                        <div id="network" class="step">
                            <h1>Network</h1>

                            <label for="network_hostname">Hostname</label><br />
                            <input id="network_hostname" class="required"
                                type="text" name="hostname"><br>
                        </div>
                        <div id="partitioning" class="step">
                            <h1>Partitioning</h1>

                            <label for="partitioning_size">Disk size</label>
                            <br />
                            <input id="partitioning_size" class="required"
                                type="text" name="image_size" value="<?php echo DEFAULT_DISK_SIZE; ?>">
                            <br>

                            <label for="">Split</label><br /><br />
                            <div id="partitioning_split" name="split"></div>
                            <br /><br />
                            <div id="partitioning_display">Not yet updated</div>
                            <br />
                            <input type="hidden" id="partitioning_boot"
                                name="boot_size" value="0"><br />
                            <input type="hidden" id="partitioning_swap"
                                name="swap_size" value="0"><br />
                            <input type="hidden" id="partitioning_root"
                                name="root_size" value="0"><br />
                        </div>
                        <div id="packages" class="step">
                            <h1>Packages</h1>

                            <label for="packages_packages">Packages list</label>
                            <br /><br />
                            <textarea id="packages_packages" name="packages"
                                style="width: 680px; height: 330px;"><?php 
                                    echo DEFAULT_PACKAGES;
                                ?></textarea>
                            <br>
                        </div>
                        <div id="expert" class="step">
                            <h1>Expert</h1>

                            <label for="expert_use">USE flags</label>
                            <br />
                            <textarea id="expert_use" name="use"
                                style="width: 680px; height: 60px;"><?php 
                                    echo DEFAULT_USE;
                                ?></textarea>
                            <br><br />
                            <label for="expert_puse">Package USE</label>
                            <br />
                            <textarea id="expert_puse" name="puse"
                                style="width: 680px; height: 60px;"><?php 
                                    echo DEFAULT_PACKAGE_USE;
                                ?></textarea>
                            <br><br />
                            <label for="expert_features">Features</label>
                            <br />
                            <textarea id="expert_features" name="features"
                                style="width: 680px; height: 60px;"><?php 
                                    echo DEFAULT_FEATURES;
                                ?></textarea>
                            <br><br />
                            <label for="expert_keywords">Accept Keywords</label>
                            <br />
                            <textarea id="expert_keywords" name="keywords"
                                style="width: 680px; height: 60px;"><?php 
                                    echo DEFAULT_KEYWORDS;
                                ?></textarea>
                            <br>
                        </div>
                        <div id="format" class="step submit_step">
                            <h1>Image format</h1>

                            <label for="format_format">Output format</label>
                            <br />
                            <select id="format_format" class="required"
                                name="format">
                                <option value="raw">Raw</option>
                                <option value="vmware">VirtualBox</option>
                                <option value="vbox">VMWare</option>
                            </select>
                            <br>
                        </div>
                        <div id="complete" class="step">
                            <h1>That's it!</h1>
                            <p>That's all there is to it! We've sent your 
                            configuration down to the kitchen to be cooked, and 
                            we'll email you once it's finished cooking.</p>
                        </div>
                    </div>
                    <div id="navigation">
                        <button id="forwardbutton" class="navigationbutton"
                            type="submit">Next</button>
                        <button id="backbutton" class="navigationbutton"
                            type="reset">Back</button>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript">
            MAX_DISK_SIZE = <?php echo MAX_DISK_SIZE; ?>;
        </script>
        <script type="text/javascript" src=" /js/gentoaster.js"></script>
    </body>
</html>