<?php
	require_once "config.php";
	require_once GENTOASTER_PATH."/ui/testdrive.php";
?>
<html>
    <head>
        <title>Gentoaster</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css"
          href="css/ui-lightness/jquery-ui-1.8.14.custom.css">
        <script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.14.js"></script>
        <script type="text/javascript" src="include/vnc.js"></script>
    </head>
    <body>
        <div id="container" style="height: 690px;">
            <div id="header"></div>
            <div id="content" style="background: #ebeae8; height:600px;">
                            <center>
                            <div id="VNC_screen">
                                <div id="VNC_status_bar" class="VNC_status_bar"
                                    style="margin-top: -25px; display:none;">
                                    <table border=0 width="100%"><tr>
                                        <td>
                                            <div id="VNC_status"
                                                style="display: none;">
                                                Loading
                                            </div>
                                        </td>
                                        <td width="1%"><div id="VNC_buttons">
                                            <input type=button
                                                value="Send CtrlAltDel"
                                                id="sendCtrlAltDelButton">
                                                </div></td>
                                    </tr></table>
                                </div>
                                <canvas id="VNC_canvas" width="800px"
                                    height="600px">
                                    Canvas not supported.
                                </canvas>
                            </div>
                            </center>
                    
                            <script>
                            /*jslint white: false */
                            /*global window, $, Util, RFB, */
                            "use strict";
                    
                            var rfb;
                    
                            function setPassword() {
                                rfb.sendPassword($D('password_input').value);
                                return false;
                            }
                            function sendCAD() {
                                rfb.sendCtrlAltDel();
                                return false;
                            }
                            function updateState(rfb, state, oldstate, msg) {
                                var s, sb, cad, level, attr;
                                s = $D('VNC_status');
                                sb = $D('VNC_status_bar');
                                cad = $D('sendCtrlAltDelButton');
                                switch (state) {
                                    case 'failed':
                                        level = "error";
                                        break;
                                    case 'fatal':
                                        level = "error";
                                        break;
                                    case 'normal':
                                        level = "normal";
                                        break;
                                    case 'disconnected':
                                        level = "normal";
                                        break;
                                    case 'loaded':
                                        level = "normal";
                                        break;
                                    default:
                                        level = "warn";
                                        break;
                                }
                    
                                if (state === "normal") {
                                    cad.disabled = false;
                                } else {
                                    cad.disabled = true;
                                }
                    
                                if (typeof(msg) !== 'undefined') {
                                    attr = "VNC_status_" + level;
                                    sb.setAttribute("class", attr);
                                    s.innerHTML = msg;
                                }
                            }
                    
                            function connect() {
                                var host, port, password;
                    
                                $D('sendCtrlAltDelButton').onclick = sendCAD;
                    
                                host = "<?php echo $server[0]; ?>";
                                port = <?php echo $server[1]; ?>;
                                password = "";
                    
                                function gqv(a,b) {
                                    return WebUtil.getQueryVar(a,b);
                                }
                    
                                rfb = new RFB({'target':$D('VNC_canvas'),
                                'encrypt': gqv('encrypt',false),
                                'true_color': gqv('true_color',true),
                                'local_cursor': gqv('cursor',true),
                                'shared': gqv('shared',true),
                                'updateState':  updateState});
                                
                                rfb.connect(host, port, password);
                                
                            };
                            
                            setTimeout("connect()", 2000);
                            </script>
            </div>
        </div>
        </script>
    </body>
</html>