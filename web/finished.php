<?php

$buildID = filter_input(INPUT_GET, "uuid", FILTER_UNSAFE_RAW);
$buildID = urlencode($buildID);

?>
<html>
	<head>
		<title>Gentoaster</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<div id="container">
			<div id="header"></div>
			<div id="content">
				<div id="main">
					<div id="finished" class="step">
						<h1>That's it!</h1>
						<p>
							That's all there is to it! We've sent your configuration down to 
							the kitchen to be toasted, come back later to get your image.
						</p>
						<p>
							You can also click 
							<a href="status.php?uuid=<?php echo $buildID; ?>">here</a> to 
							view the status of your build.
						</p>
					</div>
				</div>
				<div id="navigation">

				</div>
			</div>
		</div>
		</script>
	</body>
</html>