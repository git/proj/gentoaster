$(function(){
	$("#wizard").formwizard({ 
		validationEnabled: true,
		focusFirstInput: true,
		disableUIStyles: true,
		//historyEnabled: true,
		validationOptions: {
			rules: {
				username: {
					minlength: 2
				},
				password: {
					minlength: 5
				},
				confirmpassword: {
					minlength: 5,
					equalTo: "#user_password"
				},
				boot_size: {
					min: 32
				},
				swap_size: {
					min: 16
				},
				root_size: {
					min: 3072
				},
				image_size: {
				    min: 4020,
				    max: MAX_DISK_SIZE
				},
			},
			messages: {
				username: {
					minlength: "Your username must consist of at least 2 characters"
				},
				password: {
					minlength: "Your password must be at least 5 characters long"
				},
				confirmpassword: {
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password in both boxes"
				},
				boot_size: {
					min: "Your boot partition must be at least 32MB"
				},
				swap_size: {
					min: "Your swap partition must be at least 128MB"
				},
				root_size: {
					min: "Your root partition must be at least 3072MB"
				},
				image_size: {
				    min: "Your disk image must be at least 4020MB",
				    max: "Your disk image cannot be larger than "+MAX_DISK_SIZE+"MB"
				},
			}
		}
	});
	
	function partitioning_update(event, ui) {
			if(!ui.values) {
				ui.values = ui;
			}
	        $('.partitionrange').css('width', $('.ui-slider-range').css('left'));
	        var boot_size = ui.values[0];
	        var swap_size = ui.values[1]-ui.values[0];
	        var root_size = $("#partitioning_split").slider( "option", "max" )-ui.values[1];
	        var image_size = boot_size+swap_size+root_size;
	        $("#partitioning_display").html("Disk image size: "+image_size+"MB<br>Boot partition size: "+boot_size+"MB<br>Swap partition size: "+swap_size+"MB<br>Root partition size: "+root_size+"MB");
	        $("#partitioning_boot").attr("value", boot_size);
	        $("#partitioning_swap").attr("value", swap_size);
	        $("#partitioning_root").attr("value", root_size);
	        
	        //since it doesn't seem to redraw by itself, hacky fix!
	        $("#partitioning_split").slider("option", "values", $("#partitioning_split").slider("option", "values"));	
	}
	
	$("#partitioning_split").slider({
	    range: true,
	    min: 0,
	    max: 4096,
	    values: [128, 640],
	    slide: partitioning_update,
	    change: partitioning_update,
	    create: function(event, ui) {
	        var slider = $(event.target);
	        var range = slider.find('.ui-slider-range');
	        var newrange = $('<div />');
	        newrange.appendTo(slider).addClass('ui-slider-range partitionrange').css('width', range.css('left'));
	    }
	});
	
	function update_slider() {
		var new_size = $("#partitioning_size").val();
		$("#partitioning_split").slider("option", "max", new_size);
		partitioning_update(null, $("#partitioning_split").slider("option", "values"));
	}

	
	$("#partitioning_size").change(update_slider);
	
	partitioning_update(null, $("#partitioning_split").slider("option", "values"));
});