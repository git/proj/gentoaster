<?php

    // Gentoaster web interface settings
    // Licensed under GPL v3, see COPYING file
    
    // Path to the Gentoaster binaries/resources
    define("GENTOASTER_PATH", "/usr/share/gentoaster");
    
    // Path to the zonetab file
    define("ZONETAB", "/usr/share/zoneinfo/zone.tab");
    
    // Should we allow multiple simultaneous builds for a user?
    define("SIMULTANEOUS_BUILDS", false);
   
    // What should we set as the default keyboard layout?
    define("DEFAULT_KEYBOARD", "uk");

    // What should we set as the default timezone?
    define("DEFAULT_TIMEZONE", "Europe/London");
    
    // What should we set as the default list of packages?
    define("DEFAULT_PACKAGES", "dhcpcd");

    // What should we set as the default list of packages?
    define("DEFAULT_USE", "");
    
    // What should we set as the default list of packages?
    define("DEFAULT_PACKAGE_USE", "");
    
    // What should we set as the default list of packages?
    define("DEFAULT_FEATURES", "parallel-fetch userfetch userpriv getbinpkg");
    
    // What should we set as the default list of packages?
    define("DEFAULT_KEYWORDS", "");
    
    // What should the default virtual machine disk size be?
    define("DEFAULT_DISK_SIZE", 8192);
    
    // What should we limit the virtual machine disk size to?
    define("MAX_DISK_SIZE", 16384);
    
    // Set the RECAPTCHA keys that should be used, if enabled
    define("RECAPTCHA_ENABLED", false);
    define("RECAPTCHA_PUBLIC_KEY", "REPLACE_ME");
    define("RECAPTCHA_PRIVATE_KEY", "REPLACE_ME");
    
    // Load all the configuration for the daemon, too
    require_once GENTOASTER_PATH."/config.php";
