<?php

    // Gentoaster build daemon settings
    // Licensed under GPL v3, see COPYING file
    
    // Set the publically viewable URL for this Gentoaster instance
    define("GENTOASTER_URL", "http://192.168.2.169");

    // Set the publically viewable URL for images to be downloaded
    // This should point to the same folder as CONFIGURATIONS_PATH
    define("IMAGES_URL", "http://192.168.2.169/images");
    
    // Set the path that completed images should be stored at
    define("CONFIGURATIONS_PATH", "/var/www/gentoaster/images");
    
    // Set the path to the folder the Gentoaster tool is at
    // As long as we're not being used by the WebUI
    if (!defined("GENTOASTER_PATH")) {
        define("GENTOASTER_PATH", "/usr/share/gentoaster");
    }
    
    // Set the name of the image creation tool
    define("BUILD_TOOL_NAME", "create_image.sh");
    
    // Set the name of the testdrive wrapper
    define("WRAP_TOOL_NAME", "wrap.sh");
    
    // Set the externally accessible IP/host of this machine
    define("EXTERNAL_HOST", "192.168.2.169");

    // How much memory (in megabytes) should each testdrive instance have?
    define("TESTDRIVE_MEMORY", "512");
    
    // Set the port range that should be used for testdrives
    define("LOW_PORT", 5900);
    define("HIGH_PORT", 5999);
    
    // Set the MySQL access details that should be used
    define("MYSQL_HOSTNAME", "localhost");
    define("MYSQL_USERNAME", "gentoaster");
    define("MYSQL_PASSWORD", "");
    define("MYSQL_DATABASE", "gentoaster");
    
    // Set the SMTP details that should be used for notifications
    define("SMTP_ENABLED", false);
    define("SMTP_HOST", "");
    define("SMTP_USERNAME", "");
    define("SMTP_PASSWORD", "");
    define("SMTP_EMAIL", "");