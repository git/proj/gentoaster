<?php

    // Gentoaster build daemon status
    // Licensed under GPL v3, see COPYING file
    
    require_once "config.php";

    if (!isset($argv[1])) {
        die("No handle hash given\n");
    }

    $db = new mysqli(
        MYSQL_HOSTNAME, 
        MYSQL_USERNAME,
        MYSQL_PASSWORD, 
        MYSQL_DATABASE
    );
        
    if (mysqli_connect_errno()) {
       die("Could not connect to database ".mysqli_connect_error());
    }

    $query = "SELECT handle FROM builds WHERE id = ?";
    $stmt = $db->prepare($query);
    $stmt->bind_param("s", $argv[1]);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows == 1) {
        $stmt->bind_result($handle);
        $stmt->fetch();
        $stmt->close();
        $client = new GearmanClient();
        $client->addServer();

        $status = $client->jobStatus($handle);
        if ($status[0]) {
            if ($status[3] != 0) {
                echo "Running: " . ($status[1] ? "true" : "false");
                echo ", progress: ".ceil($status[2]/$status[3]*100) . "%, ";
                echo $status[2] . "/" . $status[3] . "\n";
            } else {
                echo "Task has not yet been processed\n";
            }
        } else {
            $query = "SELECT returncode, result FROM builds ".
                     "WHERE id = ?";
            $stmt = $db->prepare($query);
            $stmt->bind_param("s", $argv[1]);
            $stmt->execute();
            $stmt->bind_result($returncode, $result);
            $stmt->fetch();
            if ($returncode !== null) {
                echo "Job returned with code ".$returncode.": ".$result."\n";
            } else {
                echo "Job failed\n";
            }
        }
    } else {
        echo "Invalid handle hash\n";
    }
    
    $db->close();