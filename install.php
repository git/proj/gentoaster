<?php

    // Gentoaster installer
    // Licensed under GPL v3, see COPYING file

    // Check we're running as root
    $processUser = posix_getpwuid(posix_geteuid());
    if($processUser['name'] != "root") {
        die("This installer must be run as root\n");
    }

    // Load the configurations (the web config chainloads the daemon config)
    echo "Loading configuration\n";
    require_once "web/config.php";

    // Basic configuration path sanity checks
    echo "Checking configuration sanity\n";
    if(!file_exists(GENTOASTER_PATH."/install.php")) {
        die("GENTOASTER_PATH is set incorrectly\n");
    }

    // Check database configuration is sane
    echo "Connecting to database\n";
    $db = new mysqli(
        MYSQL_HOSTNAME,
        MYSQL_USERNAME,
        MYSQL_PASSWORD
    );

    if (mysqli_connect_errno()) {
       die("Could not connect to MySQL server: ".mysqli_connect_error()."\n");
    }

    // If we reached here, MySQL details are fine
    echo "Database connection is OK\n";


    // Make sure we have a dump file to use
    if (file_exists(GENTOASTER_PATH."/gentoaster.sql")) {
        // Prep MySQL details for shell args
            $mysqlHostname = escapeshellarg(MYSQL_HOSTNAME);
            $mysqlUsername = escapeshellarg(MYSQL_USERNAME);
            $mysqlDatabase = mysql_escape_string(MYSQL_DATABASE);

            // Create shell component for password if required
            if (strlen(MYSQL_PASSWORD) > 0) {
                $mysqlPassword = " --password=".escapeshellarg(MYSQL_PASSWORD)." ";
            } else {
                $mysqlPassword = " ";
            }

            // Drop any existing database here
            system('echo "DROP DATABASE IF EXISTS '.$mysqlDatabase.';" | mysql --user='.$mysqlUsername.$mysqlPassword);

            // Create a new database
            system('echo "CREATE DATABASE '.$mysqlDatabase.';" | mysql --user='.$mysqlUsername.$mysqlPassword);

            // Import the database dump into that database
            system('mysql --user='.$mysqlUsername.$mysqlPassword.$mysqlDatabase.' < gentoaster.sql');
    } else {
        die("SQL file not found, check GENTOASTER_PATH\n");
    }

    // Add the initscript for the daemon
    echo "Adding initscript symlink\n";
    system("rm -f /etc/init.d/gentoaster");
    system("ln -s ".GENTOASTER_PATH."/gentoaster /etc/init.d/gentoaster");
    system("chmod +x ".GENTOASTER_PATH."/gentoaster");

    echo "Starting Gentoaster daemon\n";
    exec("/etc/init.d/gentoaster start");

        echo "If you didn't see any errors, the install was successful\n";
        echo "Try visiting the site at ".GENTOASTER_URL." to make sure everything works\n";
