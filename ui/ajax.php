<?php

    // Gentoaster web interface AJAX remote
    // Licensed under GPL v3, see COPYING file

    require_once "config.php";

    $buildID = filter_input(INPUT_GET, "uuid", FILTER_UNSAFE_RAW);


    $db = new mysqli(
        MYSQL_HOSTNAME, 
        MYSQL_USERNAME,
        MYSQL_PASSWORD, 
        MYSQL_DATABASE
    );
    
    if (mysqli_connect_errno()) {
       die("Could not connect to database ".mysqli_connect_error());
    }

    $stmt = $db->prepare("SELECT handle FROM builds WHERE id = ?");
    $stmt->bind_param("s", $buildID);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows == 1) {
            $stmt->bind_result($handle);
            $stmt->fetch();
            $stmt->close();
            $client = new GearmanClient();
            $client->addServer();

            $status = $client->jobStatus($handle);
            if ($status[0]) {
                    if ($status[3] != 0) {
                            // in progress
                            $ret = array("status" => 1, "num" => $status[2], "den" => $status[3]);
                    } else {
                            // not yet processed
                            $ret = array("status" => 2);
                    }
            } else {
                    $query = "SELECT returncode, result ".
                             "FROM builds WHERE id = ?";
                    $stmt = $db->prepare($query);
                    $stmt->bind_param("s", $buildID);
                    $stmt->execute();
                    $stmt->bind_result($returncode, $result);
                    $stmt->fetch();
                    $stmt->close();
                    if ($returncode !== null) {
                            if ($returncode == 0) {
                                 // finished
                                 $ret = array("status" => 0);
                            } else {
                                // returned with non-zero status code
                                 $ret = array("status" => 3);
                            }
                    } else {
                            // failed
                             $ret = array("status" => 4);
                    }
            }
    } else {
             // job not found
             $ret = array("status" => -1);
    }

    $db->close();
    
    echo json_encode($ret);
?>