<?php
    require_once "config.php";
    
    if (RECAPTCHA_ENABLED) {
        require_once GENTOASTER_PATH."/ui/recaptcha.php";
    }

    if (!SIMULTANEOUS_BUILDS) {
            $db = new mysqli(
                MYSQL_HOSTNAME, 
                MYSQL_USERNAME,
                MYSQL_PASSWORD, 
                MYSQL_DATABASE
            );
                  
            if (mysqli_connect_errno()) {
               die("Could not connect to database ".mysqli_connect_error());
            }
        
            $ipaddress = filter_input(
                INPUT_SERVER, 
                "REMOTE_ADDR",
                FILTER_VALIDATE_IP
            );
                
            $query = "SELECT id, handle ".
                     "FROM builds WHERE ipaddress = ?";
            $stmt = $db->prepare($query);
            $stmt->bind_param("s", $ipaddress);
            $stmt->execute();
            $stmt->store_result();
                        
            if ($stmt->num_rows == 1) {
                    $stmt->bind_result($buildID, $handle);
                    $stmt->fetch();
                    $client = new GearmanClient();
                    $client->addServer();
                    $status = $client->jobStatus($handle);
                    if ($status[0]) {
                        $url = "status.php?uuid=".$buildID."&simultaneous=true";
                        header("Location: ".$url);
                    }
            }
            $stmt->close();
    }

    $timezones = array();
    $zonetab = file(ZONETAB);
    foreach ($zonetab as $buf) {
        if (substr($buf, 0, 1)=='#') {
            continue;
        }
        $rec = preg_split('/\s+/', $buf);
        $key = $rec[2];
        $val = $rec[2];
        $c = count($rec);
        for ($i=3;$i<$c;$i++) {
            $val.= ' '.$rec[$i];
        }
        $timezones[$key] = $val;
        ksort($timezones);
    }
    $timezoneOption = "";
    foreach ($timezones as $timezone => $description) {
        $timezoneOption .= "<option";
        if ($timezone == DEFAULT_TIMEZONE) {
            $timezoneOption .= " selected";
        }
        $timezoneOption .= ">".$timezone."</option>\n";
    }
    $layoutLines = file(GENTOASTER_PATH."/res/keyboard.lst");
    $keyboardOption = "";
    $layouts = array();
    
    foreach($layoutLines as $layout) {
        $layoutdata = explode("\t", $layout);
        $layouts[$layoutdata[0]] = $layoutdata[1];
    }
    asort($layouts);
     
    foreach($layouts as $layoutCode => $layoutName) {   
        $keyboardOption .= "<option value=\"".$layoutCode."\"";
        if ($layoutCode == DEFAULT_KEYBOARD) {
            $keyboardOption .= " selected";
        }
        $keyboardOption .= ">".trim($layoutName)."</option>\n";
    }
?>