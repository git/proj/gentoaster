<?php

    // Gentoaster web interface config processor
    // Licensed under GPL v3, see COPYING file

    require_once "config.php";

    $ipaddress = filter_input(
        INPUT_SERVER,
        "REMOTE_ADDR",
        FILTER_VALIDATE_IP
    );

    if (RECAPTCHA_ENABLED) {
        require_once GENTOASTER_PATH."/ui/recaptcha.php";

        $challenge = filter_input(
            INPUT_POST,
            "recaptcha_challenge_field",
            FILTER_UNSAFE_RAW
        );
        
        $response = filter_input(
            INPUT_POST, 
            "recaptcha_response_field",
            FILTER_UNSAFE_RAW
        );
        
        $resp = recaptcha_check_answer(
            RECAPTCHA_PRIVATE_KEY,
            $ipaddress,
            $challenge,
            $response
        );
                                    
        if (!$resp->is_valid) {
            die("CAPTCHA was incorrect");
        }
    }

    function sanitize_shellarg($arg)
    {
        $arg = str_replace("\r\n", " ", $arg);
        $arg = str_replace("\n", " ", $arg);
        return escapeshellarg($arg);
    }
    $sfi = array("options" => "sanitize_shellarg");

    $buildID = uniqid();
    $bootMegabytes = filter_input(INPUT_POST, "boot_size", FILTER_VALIDATE_INT);
    $swapMegabytes = filter_input(INPUT_POST, "swap_size", FILTER_VALIDATE_INT);
    $rootMegabytes = filter_input(INPUT_POST, "root_size", FILTER_VALIDATE_INT);
    $timezone = filter_input(INPUT_POST, "timezone", FILTER_CALLBACK, $sfi);
    $keyboard = filter_input(INPUT_POST, "keyboard", FILTER_CALLBACK, $sfi);
    $hostname = filter_input(INPUT_POST, "hostname", FILTER_CALLBACK, $sfi);
    $username =  filter_input(INPUT_POST, "username", FILTER_CALLBACK, $sfi);
    $password = filter_input(INPUT_POST, "password", FILTER_CALLBACK, $sfi);
    $rootPass = filter_input(INPUT_POST, "rootpassword", FILTER_CALLBACK, $sfi);
    $packagesList = filter_input(INPUT_POST, "packages", FILTER_CALLBACK, $sfi);
    $use = filter_input(INPUT_POST, "use", FILTER_CALLBACK, $sfi);
    $puse = filter_input(INPUT_POST, "puse", FILTER_CALLBACK, $sfi);
    $features = filter_input(INPUT_POST, "features", FILTER_CALLBACK, $sfi);
    $keywords = filter_input(INPUT_POST, "keywords", FILTER_CALLBACK, $sfi);
    $outputFormat = filter_input(INPUT_POST, "format", FILTER_CALLBACK, $sfi);
    $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);

$iniString = "[vmconfig]

BUILD_ID='$buildID'
BOOT_MEGABYTES='$bootMegabytes'
SWAP_MEGABYTES='$swapMegabytes'
ROOT_MEGABYTES='$rootMegabytes'
TIMEZONE=$timezone
KEYBOARD=$keyboard
HOSTNAME=$hostname
ROOT_PASSWORD=$rootPass
DEFAULT_USERNAME=$username
DEFAULT_PASSWORD=$password
USE_FLAGS=$use
PACKAGE_USE=$puse
FEATURES=$features
PACKAGE_ACCEPT_KEYWORDS=$keywords
PACKAGES_LIST=$packagesList
OUTPUT_FORMAT=$outputFormat";

    $client = new GearmanClient();
    $client->addServer();
    $handle = $client->doBackground("invoke_image_build", $iniString);

    $db = new mysqli(
        MYSQL_HOSTNAME, 
        MYSQL_USERNAME,
        MYSQL_PASSWORD, 
        MYSQL_DATABASE
    );
            
    if (mysqli_connect_errno()) {
       die("Could not connect to database ".mysqli_connect_error());
    }

    $query = "INSERT INTO builds (id, handle, ipaddress, email) ".
             "VALUES(?, ?, ?, ?)";
    $stmt = $db->prepare($query);
    $stmt->bind_param("ssss", $buildID, $handle, $ipaddress, $email);
    $stmt->execute();
    $stmt->close();
    $db->close();

    header("Location: finished.php?uuid=".$buildID);