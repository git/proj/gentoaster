<?php

    // Gentoaster web interface status
    // Licensed under GPL v3, see COPYING file

    require_once "config.php";

    $buildID = filter_input(INPUT_GET, "uuid", FILTER_UNSAFE_RAW);
    $simultaneous = filter_input(
        INPUT_GET, 
        "simultaneous", 
        FILTER_VALIDATE_BOOLEAN
    );
    $bres = "Unknown!";
    $inprogress = false;
    $builddone = false;
    $simultaneousString = "";

    if ($simultaneous && !SIMULTANEOUS_BUILDS) {
        $simultaneousString = "You were redirected to this page because you ". 
                              "already have a build in progress. Simultaneous ".
                              "builds are disabled on this server.<br/><br/>";
    }

    $db = new mysqli(
        MYSQL_HOSTNAME, 
        MYSQL_USERNAME,
        MYSQL_PASSWORD, 
        MYSQL_DATABASE
    );
    
    if (mysqli_connect_errno()) {
       die("Could not connect to database ".mysqli_connect_error());
    }

    $stmt = $db->prepare("SELECT handle FROM builds WHERE id = ?");
    $stmt->bind_param("s", $buildID);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows == 1) {
            $stmt->bind_result($handle);
            $stmt->fetch();
            $stmt->close();
            $client = new GearmanClient();
            $client->addServer();

            $status = $client->jobStatus($handle);
            if ($status[0]) {
                    if ($status[3] != 0) {
                            $percentage = ceil($status[2]/$status[3]*100);
                            $bres = "Your build is currently running".
                                           " and is <span id=\"percent\">".$percentage."</span>% complete";
                            $inprogress = true;
                    } else {
                            $bres = "Task has not yet been processed";
                    }
            } else {
                    $query = "SELECT returncode, result ".
                             "FROM builds WHERE id = ?";
                    $stmt = $db->prepare($query);
                    $stmt->bind_param("s", $buildID);
                    $stmt->execute();
                    $stmt->bind_result($returncode, $result);
                    $stmt->fetch();
                    $stmt->close();
                    if ($returncode !== null) {
                            if ($returncode == 0) {
                                $bres = "Your build is complete! ".
                                   "What would you like to do now?".
                                   "<br /><br /><center>".
                                   "<table><tr><td>".
                                   "<a href=\"".IMAGES_URL."/".
                                   $buildID."/".$buildID.
                                   ".tar.gz\">".
                                   "<img style=\"padding: 10px;\" ".
                                   "src=\"img/icons/download.png\">".
                                   "</a></td><td>".
                                   "<a href=\"testdrive.php?uuid=".
                                   $buildID."\">".
                                   "<img style=\"padding: 10px;\" ".
                                   "src=\"img/icons/testdrive.png\">".
                                   "</a></td></tr>".
                                   "<tr><td>Download</td>".
                                   "<td>Testdrive</td></tr>".
                                   "</table></center>";
                                $builddone = true;
                            } else {
                                $bres = "Job returned with code ".
                                        $returncode.": ".$result;
                            }
                    } else {
                            $bres = "Job failed";
                    }
            }
    } else {
            $stmt->close();
            $bres = "Invalid handle hash";
    }

    $db->close();
    
    if (!$builddone) {
        $titleString = "How's things?";
    } else {
        $titleString = "It's showtime!";
    }
?>