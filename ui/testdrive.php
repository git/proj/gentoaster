<?php

    // Gentoaster web interface framebuffer testdrive
    // Licensed under GPL v3, see COPYING file

    require_once "config.php";

    $buildID = filter_input(INPUT_GET, "uuid", FILTER_UNSAFE_RAW);
    $buildresult = "Unknown!";
    $inprogress = false;

    $db = new mysqli(
        MYSQL_HOSTNAME, 
        MYSQL_USERNAME,
        MYSQL_PASSWORD, 
        MYSQL_DATABASE
    );
    
    if (mysqli_connect_errno()) {
       die("Could not connect to database ".mysqli_connect_error());
    }

    $stmt = $db->prepare("SELECT handle FROM builds WHERE id = ?");
    $stmt->bind_param("s", $buildID);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows == 1) {
            $stmt->bind_result($handle);
            $stmt->fetch();
            $stmt->close();
            $client = new GearmanClient();
            $client->addServer();

            $status = $client->jobStatus($handle);
            if ($status[0]) {
                    header("Location: status.php?uuid=".$buildID);
            } else {
                    $query = "SELECT returncode, result ".
                             "FROM builds WHERE id = ?";
                    $stmt = $db->prepare($query);
                    $stmt->bind_param("s", $buildID);
                    $stmt->execute();
                    $stmt->bind_result($returncode, $result);
                    $stmt->fetch();
                    $stmt->close();
                    if ($returncode !== null) {
                            if ($returncode == 0) {
                                // we're built, let's do this
                                $client = new GearmanClient();
                                $client->addServer();
                                $server = $client->do(
                                    "invoke_start_image",
                                    $buildID
                                );
                                $server = unserialize($server);
                            } else {
                                header("Location: status.php?uuid=".$buildID);
                            }
                    } else {
                               header("Location: status.php?uuid=".$buildID);
                    }
            }
    } else {
            $stmt->close();
            die("Invalid handle hash");
    }

    $db->close();
?>