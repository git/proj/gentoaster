#!/bin/bash

cd  /usr/share/websockify
MEMORY=${3}
VNCDISPLAY=$(( ${2}-5900 ))
RPORT=$(( ${2}+1000 ))
qemu -hda $1 -m ${MEMORY} -net nic -net user -vnc :$VNCDISPLAY -usbdevice tablet &
websockify -D ${RPORT} 127.0.0.1:${2}
